<?php

namespace Socfest\Tests;

use Socfest\Grammar\Helper\HungarianGrammarHelper;
use Socfest\Grammar\Twig\HungarianGrammarExtensionGrammarTest;
use PHPUnit\Framework\TestCase;

class GrammarTest extends TestCase
{
    public function testWithLastMultipleDouble() {
        // magas
        $this->assertEquals(
            'meccsel',
            HungarianGrammarHelper::withTag('meccs')
        );
        // magas
        $this->assertEquals(
            'meggyel',
            HungarianGrammarHelper::withTag('meggy')
        );

    }

    public function testWithLastDouble()
    {
        // magas
        $this->assertEquals(
            'Anettel',
            HungarianGrammarHelper::withTag('Anett')
        );

        // mély
        $this->assertEquals(
            'ottal',
            HungarianGrammarHelper::withTag('ott')
        );

    }
    public function testWithLastNormal()
    {

        // vegyes
        $this->assertEquals(
            'hangrenddel',
            HungarianGrammarHelper::withTag('hangrend')
        );
    }

    public function testWithLastMultiple() {
        // magas
        $this->assertEquals(
            'aggyal',
            HungarianGrammarHelper::withTag('agy')
        );
        // magas
        $this->assertEquals(
            'szemésszel',
            HungarianGrammarHelper::withTag('szemész')
        );

        // magas
        $this->assertEquals(
            'briddzsel',
            HungarianGrammarHelper::withTag('bridzs')
        );
    }

    public function testWithLastVowel()
    {
        // mély
        $this->assertEquals(
            'almával',
            HungarianGrammarHelper::withTag('alma')
        );
        // magas
        $this->assertEquals(
            'dinnyével',
            HungarianGrammarHelper::withTag('dinnye')
        );
        $this->assertEquals(
            'esszével',
            HungarianGrammarHelper::withTag('esszé')
        );
        // vegyes
        $this->assertEquals(
            'amivel',
            HungarianGrammarHelper::withTag('ami')
        );

    }


    /**
     * ért
     */
    public function testOn()
    {
        $this->assertEquals(
            'almán',
            HungarianGrammarHelper::onTag('alma')
        );

        $this->assertEquals(
            'budapesten',
            HungarianGrammarHelper::onTag('budapest')
        );
    }

    /**
     * ért
     */
    public function testFor()
    {
        $this->assertEquals(
            'sátorért',
            HungarianGrammarHelper::forTag('sátor')
        );
    }

    /**
     * nak/nek
     */
    public function testTo()
    {
        $this->assertEquals(
            'sátornak',
            HungarianGrammarHelper::toTag('sátor')
        );
        $this->assertEquals(
            'almának',
            HungarianGrammarHelper::toTag('alma')
        );
        $this->assertEquals(
            'dinnyének',
            HungarianGrammarHelper::toTag('dinnye')
        );
        $this->assertEquals(
            'gyümölcsnek',
            HungarianGrammarHelper::toTag('gyümölcs')
        );
    }

    /**
     * ban/ben
     */
    public function testIn()
    {
        $this->assertEquals(
            'sátorban',
            HungarianGrammarHelper::inTag('sátor')
        );
        $this->assertEquals(
            'almában',
            HungarianGrammarHelper::inTag('alma')
        );
        $this->assertEquals(
            'dinnyében',
            HungarianGrammarHelper::inTag('dinnye')
        );
        $this->assertEquals(
            'gyümölcsben',
            HungarianGrammarHelper::inTag('gyümölcs')
        );
    }

    /**
     * né
     */
    public function testMarriedName()
    {
        $this->assertEquals(
            'Béláné',
            HungarianGrammarHelper::marriedMaleName('Béla')
        );
        $this->assertEquals(
            'Sándorné',
            HungarianGrammarHelper::marriedMaleName('Sándor')
        );

    }

}
