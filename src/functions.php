<?php

use Socfest\Grammar\Helper\HungarianGrammarHelper;

function hgin($string) {
    $string = HungarianGrammarHelper::in($string);
    return $string;
}

function hgfor($string) {
    $string = HungarianGrammarHelper::for($string);
    return $string;
}

function hgto($string) {
    $string = HungarianGrammarHelper::to($string);
    return $string;
}

function hgwith($string) {
    $string = HungarianGrammarHelper::withTag($string);
    return $string;
}

function hgarticle($string) {
    $string = HungarianGrammarHelper::definiteArticle($string);
    return $string;
}
