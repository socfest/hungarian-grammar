<?php
/**
 * Created by PhpStorm.
 * User: ptibor
 * Date: 2019-02-13
 * Time: 12:56
 */

namespace Socfest\Grammar\Twig;

if (!class_exists("Twig\Extension\AbstractExtension")) {
    return;
}

use Socfest\Grammar\Helper\HungarianGrammarHelper;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class HungarianGrammarExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            // the logic of this filter is now implemented in a different class
            new TwigFilter('hgnr', [HungarianGrammarHelper::class, 'numberFormat']),
            new TwigFilter('hgin', [HungarianGrammarHelper::class, 'toTag']),
            new TwigFilter('hgto', [HungarianGrammarHelper::class, 'toTag']),
            new TwigFilter('hgfor', [HungarianGrammarHelper::class, 'forTag']),
            new TwigFilter('hgwith', [HungarianGrammarHelper::class, 'withTag']),
            new TwigFilter('hgmarried', [HungarianGrammarHelper::class, 'marriedMaleName']),
            new TwigFilter('hgarticle', [HungarianGrammarHelper::class, 'definiteArticle']),
        ];
    }
}
