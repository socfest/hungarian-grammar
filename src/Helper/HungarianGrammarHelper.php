<?php
/**
 * Created by PhpStorm.
 * User: ptibor
 * Date: 2019-02-13
 * Time: 12:43
 */

namespace Socfest\Grammar\Helper;

class HungarianGrammarHelper
{
    // magánhangzók
    static $vowels = [
        'a' => self::LOW_TONE,
        'á' => self::LOW_TONE,
        'e' => self::HIGH_TONE,
        'é' => self::HIGH_TONE,
        'i' => self::HIGH_TONE,
        'í' => self::HIGH_TONE,
        'o' => self::LOW_TONE,
        'ó' => self::LOW_TONE,
        'ö' => self::HIGH_TONE,
        'ő' => self::HIGH_TONE,
        'u' => self::LOW_TONE,
        'ú' => self::LOW_TONE,
        'ü' => self::HIGH_TONE,
        'ű' => self::HIGH_TONE,
    ];

    // rövid-hosszú dupla mássalhangzó-párok
    static $multiples = [
        'cs' => 'ccs',
        'dz' => 'ddz',
        'dzs' => 'ddzs',
        'gy' => 'ggy',
        'ly' => 'lly',
        'ny' => 'nny',
        'sz' => 'ssz',
        'ty' => 'tty',
        'zs' => 'zzs'
    ];

    // rövid-hosszú magánhangzó-párok
    static $vowelPairs = [
        'a' => 'á',
        'e' => 'é',
//        'i' => 'í', // az i betű nem szokott hosszabbodni
        'o' => 'ó',
        'ö' => 'ő',
        'u' => 'ú',
        'ü' => 'ű',
    ];

    // hangrendek
    const
        LOW_TONE = -1,
        MID_TONE = 0,
        HIGH_TONE = 1
    ;

    /**
     * A/Az
     *
     * @param $string
     * @return string
     */
    public static function definiteArticle($string)
    {
        return strtolower('A' . (self::firstLetterVowel($string) ? 'z' : ''));
    }

    /**
     * @param $string
     *
     * budapesten
     * érden
     * győrött
     * szexárdon
     *
     */
    public static function onTag($string)
    {
        if (self::lastLetterVowel($string)) {
            $string = self::longLastVowel($string);
            $string .= 'n';
            return $string;
        }

        if (self::wordTone($string) == self::MID_TONE) {
            $string = $string.'on';
        } else {
            $string = $string.'en';
        }

        return $string;
    }

    /**
     * Ért
     *
     * @param $string
     * @return string
     */
    public static function forTag($string) {
        $string = self::longLastVowel($string);
        return $string.'ért';
    }

    /**
     * Nak/nek
     *
     * @param $string
     * @return string
     */
    public static function toTag($string) {
        $tags = [
            self::LOW_TONE => 'nak',
            self::MID_TONE => 'nek',
            self::HIGH_TONE => 'nek'
        ];

        $string = self::longLastVowel($string);

        return $string.$tags[self::wordTone($string)];
    }

    /**
     * Ban/ben
     *
     * @param $string
     * @return string
     */
    public static function inTag($string)
    {
        $tags = [
            self::LOW_TONE => 'ban',
            self::MID_TONE => 'ben',
            self::HIGH_TONE => 'ben'
        ];

        $string = self::longLastVowel($string);

        return $string.$tags[self::wordTone($string)];
    }

    /**
     * Asszonynevesítés (Béla => Béláné)
     *
     * @param $string
     * @return string
     */
    public static function marriedMaleName($string) {
        $string = self::longLastVowel($string);
        return $string.'né';
    }

    /**
     * Val/vel
     *
     * @param $string
     * @return string|string[]|null
     */
    public static function withTag($string)
    {
        $tags = [
            self::LOW_TONE => 'val',
            self::MID_TONE => 'vel',
            self::HIGH_TONE => 'vel'
        ];

        return self::completeResemblance(
            $string,
            $tags[self::wordTone($string)]
        );
    }

    /**
     * Magyar számformátum
     *
     * @param $number
     * @param int $decimals
     * @param string $dec_point
     * @param string $thousands_sep
     * @return string
     */
    public static function numberFormat($number , $decimals = 0 , $dec_point = ',' , $thousands_sep = ' ' ) {
        return number_format($number , $decimals, $dec_point, $thousands_sep);
    }

    /**
     * Az utolsó magánhangzót hosszúvá alakítja a toldalékolás miatt
     *
     * @param $string
     * @return string
     */
    protected static function longLastVowel($string)
    {
        if (HungarianGrammarHelper::lastLetterVowel($string)) {
            $lastLetter = substr($string, -1);
            if (in_array(strtolower($lastLetter), array_keys(self::$vowelPairs))) {
                $string = substr($string, 0, strlen($string) - 1) . self::$vowelPairs[strtolower($lastLetter)];
            }
        }
        return $string;
    }

    /**
     * Eldönti a szó hangrendjét
     *
     * mély hangrendű magánhangzók: a, á, o, ó, u és ú
     * magas hangrendű magánhangzók: e, é, i, í, ö, ő, ü és ű
     *
     * Amennyiben csak mély magánhangzókat tartalmaz akkor mély-,
     * ha csak magasakat akkor magas-, ha különböző hangrendű
     * magánhangzókat tartalmaz egy szó, akkor vegyes hangrendűnek
     * tekintjük
     *
     * @param $string
     * @return int
     */
    public static function wordTone($string)
    {
        $lowVowels = join(array_keys(array_filter(self::$vowels, function ($v) { return $v == self::LOW_TONE; })));
        $highVowels = join(array_keys(array_filter(self::$vowels, function ($v) { return $v == self::HIGH_TONE; })));

        $lows = preg_match_all('/[' . $lowVowels . ']/ui', $string);
        $highs = preg_match_all('/[' . $highVowels . ']/ui', $string);

        return $highs <=> $lows;
    }

    /**
     * Magánhangzóval kezdődik a szó?
     *
     * @param $string
     * @return bool
     */
    protected static function firstLetterVowel($string)
    {
        return preg_match('/^['.join('',array_keys(self::$vowels)).']{1}/iu', $string) ? true : false;
    }

    /**
     * Magánhangzóra végződik a szó?
     *
     * @param $string
     * @return bool
     */
    protected static function lastLetterVowel($string)
    {
        return preg_match('/['.join('',array_keys(self::$vowels)).']{1}$/iu', $string) ? true : false;
    }

    /**
     * Rövid, dupla mássalnagzóra végződik a szó?
     *
     * @param $string
     * @return bool
     */
    protected static function lastLetterMultiple($string)
    {
        return preg_match('/('.join(')|(',array_keys(self::$multiples)).')$/iu', $string) ? true : false;
    }

    /**
     * Hosszú, dupla mássalnagzóra végződik a szó?
     *
     * @param $string
     * @return bool
     */
    protected static function lastLetterMultipleDouble($string)
    {
        return preg_match('/('.join(')|(',array_values(self::$multiples)).')$/iu', $string) ? true : false;
    }

    /**
     * Hosszú mássalnagzóra végződik a szó?
     *
     * @param $string
     * @return bool
     */
    protected static function lastLetterDoubleConsonant($string) {
        $lastLetter = self::getLastLetter($string);
        return preg_match('/'.$lastLetter.$lastLetter.'$/i', $string) ? true : false;
    }

    /**
     * @param string $string
     * @param string $tag
     * @return string
     */
    protected static function completeResemblance($string, $tag)
    {
        $tagFirstLetter = substr($tag, 0, 1);
        $original = $string.$tag;
        if (self::lastLetterVowel($string)) {
            $string = self::longLastVowel($string);
            return $string.$tag;
        } elseif (self::lastLetterDoubleConsonant($string)) {
            return $string.trim($tag,$tagFirstLetter);
        } elseif(self::lastLetterMultipleDouble($string)) {
            return $string.trim($tag,$tagFirstLetter);
        } elseif (self::lastLetterMultiple($string)) {
            $replacePairs = [];
            foreach (self::$multiples as $short => $long) {
                $replaced = preg_replace('/([' . $short . ']{' . strlen($short) . '}v)([e|a]l)$/i', $long . '$2', $original);
                if ($original != $replaced) {
                    return $replaced;
                }
            }
        } else {
            $replaced = preg_replace('/([a-z]{1})v([e|a]l)$/i', '$1$1$2', $original);
            return $replaced;
        }
    }

    /**
     * @param $string
     * @return bool|string
     */
    protected static function getLastLetter($string)
    {
        return substr($string, -1, 1);
    }


    public static function ordering($a, $b)
    {
        $i = array(
            'Á', 'á', 'É', 'é', 'Í', 'í', 'Ó', 'ó', 'Ö', 'ö', 'Ő', 'ő', 'Ú', 'ú', 'Ü', 'ü', 'Ű', 'ű', //ékezetesek
            'Dr.', 'dr.', 'Ifj.', 'ifj.', //előtagok
            'Cs', 'cs', 'Dz', 'Dzs', 'Gy', 'gy', 'Ly', 'ly', 'Ny', 'ny', 'Sz', 'sz', 'Ty', 'ty', 'Zs', 'zs' //kettős betűk (Kezdőbetűknél a Cs-sek a C és a D sor közt kell, hogy legyenek. A köztes betűknél a cs megelőzi pl. a ct cu tartalmakat
        );
        $o = array(
            'A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'O', 'o', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u',
            '', '', '', '',
            'Czzz', 'cs', 'Dzz', 'Dzzz', 'Gzz', 'gg', 'Lzz', 'll', 'Nzz', 'nn', 'Szz', 'ss', 'Tzz', 'tt', 'Zzz', 'zz'
        );

        $a = str_replace($i, $o, $a);
        $b = str_replace($i, $o, $b);

        return strcmp(trim($a), trim($b));
    }
}
