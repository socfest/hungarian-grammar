# Magyar helyesírási segéd
Ez a csomag segítséget nyújt a helyes ragozásban és névelőhasználatban az általános magyar helyesírási szabályok alapján

- határozott névelő *(a/az)*
- -ban/-ben
- -ért
- -nak/-nek
- -val/-vel
- -né *(asszonynevek pl: Lászlóné)*

***Fontos megjegyezsni**, hogy a magyar nyelvi szabályokban előforduló kivételeket a program nem kezeli*
## Telepítés
````
composer require socfest/hungarian-grammar
````
Amennyiben szeretnéd használni a Twig függvényeket:

````
# config/services.yml

services:
    Socfest\Grammar\Twig\HungarianGrammarExtension:
        tags: ["twig.extension"]
````
## Használat
**static method**

````
# a/az
$string = HungarianGrammarHelper::definiteArticle($string)

# ban/ben
$string = HungarianGrammarHelper::inTag($string)
# ért
$string = HungarianGrammarHelper::forTag($string)
# nak/nek
$string = HungarianGrammarHelper::toTag($string)
# val/vel
$string = HungarianGrammarHelper::withTag($string)
# né
$string = HungarianGrammarHelper::marriedMaleName($string)

````

**function**
````
$string = hgarticle($string); 

$string = hgin($string);
$string = hgfor($string);
$string = hgto($string);
$string = hgwith($string);
$string = hgmarried($string);
````

**Twig** 
````
{{'string'|hgarticle}}

{{'string'|hgin}}
{{'string'|hgfor}}
{{'string'|hgto}}
{{'string'|hgwith}}
{{'string'|hgmarried}}
````
